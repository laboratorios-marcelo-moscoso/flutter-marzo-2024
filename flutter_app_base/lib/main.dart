import 'package:flutter/material.dart';
import 'package:flutter_app_base/screens/about_screen.dart';
import 'package:flutter_app_base/screens/alert_dialog_screen.dart';
import 'package:flutter_app_base/screens/futurama_screen.dart';
import 'package:flutter_app_base/screens/home_screen.dart';
import 'package:flutter_app_base/screens/listview_screen.dart';
import 'package:flutter_app_base/screens/snackbar_screen.dart';

void main() {
  runApp(
    MaterialApp(
      theme: ThemeData(
        fontFamily: 'Roboto',
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.teal,
          foregroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.white)
        )
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => const HomeScreen(),
        '/about': (context) => const AboutScreen(),
        '/snackbar': (context) => const SnackBarScreen(),
        '/alertdialog': (context) => const AlertDialogScreen(),
        '/futurama': (context) => const FuturamaScreen(),
        '/listview': (context) => const ListViewScreen(),

      }
    ),
  );
}

class FirstScreen extends StatelessWidget {
  const FirstScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('First Screen'),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/second');
          },
          child: const Text('Launch screen'),
        ),
      ),
    );
  }
}

class SecondScreen extends StatelessWidget {
  const SecondScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Second Screen'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text('Go back!'),
        ),
      ),
    );
  }
}