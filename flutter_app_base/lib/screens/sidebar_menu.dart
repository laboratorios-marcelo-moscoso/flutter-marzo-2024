import 'package:flutter/material.dart';

class SideBarMenu extends StatelessWidget {
  const SideBarMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.teal
            ),
            child: Text(
              'Menu',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24
              ),
            )
          ),
          ListTile(
            leading: const Icon(Icons.home),
            title: const Text('Home'),
            onTap: (){
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/');

            },
          ),
          ListTile(
            leading: const Icon(Icons.people),
            title: const Text('About'),
            onTap: (){
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/about');

            },
          ),
          ListTile(
            leading: const Icon(Icons.message),
            title: const Text('SnackBar'),
            onTap: (){
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/snackbar');

            },
          ),
          ListTile(
            leading: const Icon(Icons.window),
            title: const Text('AlerDialog'),
            onTap: (){
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/alertdialog');

            },
          ),
          ListTile(
            leading: const Icon(Icons.movie),
            title: const Text('Futurama'),
            onTap: (){
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/futurama');

            },
          ),
          ListTile(
            leading: const Icon(Icons.list),
            title: const Text('ListView'),
            onTap: (){
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/listview');

            },
          ),
        ],
      ),
    );
  }
}