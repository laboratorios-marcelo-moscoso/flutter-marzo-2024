import 'package:flutter/material.dart';
import 'package:flutter_app_base/screens/sidebar_menu.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class FuturamaScreen extends StatefulWidget {
  const FuturamaScreen({super.key});

  @override
  State<FuturamaScreen> createState() => _FuturamaScreenState();
}

class _FuturamaScreenState extends State<FuturamaScreen> {

  Future<List> _fetchFuturama() async {
    final resp = await http.get(Uri.parse('https://api.sampleapis.com/futurama/characters'));
    if (resp.statusCode == 200) {
      return json.decode(resp.body);
    } else {
      throw Exception('Failed to load album');
    }
  }

    @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Futurama Characters (FutureBuilder)'),
      ),
      drawer: const SideBarMenu(),
      body: FutureBuilder(
          future: _fetchFuturama(),
          builder: (BuildContext context,AsyncSnapshot snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return const Center(child: CircularProgressIndicator(),);
            }else{
              var characters = snapshot.data;
              return ListView.builder(
                itemCount: characters.length,
                itemBuilder: (context, index) {
                final character = characters[index];
                return ListTile(
                    leading: Container(
                      width: 80,
                      height: 180,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(character['images']['main']),
                        ),
                      ),
                    ),
                    title: Text(character['name']['first']),
                    subtitle: Text(character['species']),
                  );
                },
              );
            }
          },
        )
    );
  }



}