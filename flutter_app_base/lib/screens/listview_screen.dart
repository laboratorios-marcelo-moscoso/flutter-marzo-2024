import 'package:flutter/material.dart';
import 'package:flutter_app_base/screens/sidebar_menu.dart';

class ListViewScreen extends StatefulWidget {
  const ListViewScreen({super.key});

  @override
  State<ListViewScreen> createState() => _ListViewScreenState();
}

class _ListViewScreenState extends State<ListViewScreen> {

  TextEditingController _nameController = TextEditingController();
  List<String> names = [];

  void addName() {
    setState(() {
      String newName = _nameController.text;
      if (newName.isNotEmpty) {
        names.add(newName);
        _nameController.clear();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      drawer: const SideBarMenu(),
      appBar: AppBar(
        title: const Text('ListView Example'),
      ),
      body: ListView.builder(
        itemCount: names.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(names[index]),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('Add Name'),
              content: TextField(
                controller: _nameController,
                decoration: const InputDecoration(labelText: 'Enter a name'),
              ),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    addName();
                    Navigator.of(context).pop();
                  },
                  child: const Text('Add'),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Cancel'),
                ),
              ],
            ),
          );
        },
        tooltip: 'Add Name',
        child: const Icon(Icons.add),
      ),
    );
  }


}